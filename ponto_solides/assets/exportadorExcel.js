	/**
    * EXPORTAR TABELA PARA O EXCEL
	LINK
	<img " src="<?php echo CAMINHO_RAIZ;?>/imagens/excel.gif" title="Exportar para Excel" class="exportarExcel" idTabela="resultado" nomeArquivo="COLABORADORES>

	HTML: 
	para não aparecer coluna no excel, acrescentar no th: removeExcel
	
	<table class="tablesorter" id="resultado" name="resultado" width="100%">
		<thead align="center">
			<th style="width:5%;">Chapa Colaborador</th>
			<th style="width:30%;">Nome Colaborador</th>
			<th style="width:15%;">Email</th>
			<th removeExcel style="width:5%;">Editar Email</th>
		</thead>
		<tbody align="center" class="lista">
		</tbody>
	</table>
    */
	
	
    $(document).on('click', '.exportarExcel', function() {
    
       

        var idTabela    = '#' + $(this).attr('idTabela');
        var nomeArquivo = $(this).attr('nomeArquivo');
        var nth         = "";

        $(idTabela).clone().attr('id', 'tabelaExportarExcel').insertAfter(idTabela);

        $('#tabelaExportarExcel thead tr').children().each (function() {

            if(typeof $(this).attr('removeExcel') !== 'undefined' && $(this).attr('removeExcel') !== false) {

                nth = $(this).index() + 1;
                
                $('#tabelaExportarExcel th:nth-child(' + nth + ')').remove();
                $('#tabelaExportarExcel td:nth-child(' + nth + ')').remove();
            }

            if($(this).is(':hidden') && (typeof $(this).attr('exibeExcel') === 'undefined' || $(this).attr('exibeExcel') === false)) {

                nth = $(this).index() + 1;

                $('#tabelaExportarExcel th:nth-child(' + nth + ')').remove();
                $('#tabelaExportarExcel td:nth-child(' + nth + ')').remove();

            }
                    
        });

        $("#tabelaExportarExcel").attr('border','1');
        $("#tabelaExportarExcel tr td img").remove();
        $("#tabelaExportarExcel tr").removeAttr('style');

        var exportar        = document.createElement('a');
        var tipo_arquivo    = 'data:application/vnd.ms-excel';
        var tabela          = document.getElementById('tabelaExportarExcel');
        var tabela_html     = '<meta http-equiv="content-type" content="text/html; charset=utf-8" />';

        tabela_html         += tabela.outerHTML.replace(/ /g, '%20');

        exportar.href       = tipo_arquivo + ', ' + tabela_html;
        exportar.charset    = "UTF-8";
        exportar.download   = nomeArquivo + '.xls';
        document.body.appendChild(exportar);
        exportar.click();
        document.body.removeChild(exportar);        
        $("#tabelaExportarExcel").remove();
        
   });