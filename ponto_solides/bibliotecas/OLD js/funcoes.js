/**
 * @author Welington
 */
function calculaQtd($value,$preco,$qtd){
	$value= parseInt($value);
	$preco= parseFloat($preco.toFixed(2));
	$qtd= parseInt($qtd);
	var text;
	var reg = /[^0-9_]/gi; //apenas números
	if ($value>$qtd){
		alert("Quantidade acima do estoque");
		document.getElementById('qtd').value="";
		text= "<strong>Total:</strong>";
	}
	else if(reg.test($value)){
		alert("Digite apenas números");
		document.getElementById('qtd').value="";
		text= "<strong>Total: </strong>";
	}
	else if(!isNaN($value) && $value>0){
		$total= $value *$preco;
		$total= $total.toFixed(2);
		text= "<strong>Total: R$</strong>";
		text += $total;
	}
	else{
		text= "<strong>Total: </strong>";
	}
	document.getElementById('subtotal').innerHTML= text;
}//fim de calculaQtd()

function limpa(){
	document.getElementById('subtotal').innerHTML= "<strong>Total: </strong>";
}//limpa()
