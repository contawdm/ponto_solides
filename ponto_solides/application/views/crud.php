<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$this->load->view("includes/header.php");
//$this->load->view("includes/menu.php");

?>

	
        <div id="container">
            <?php 
                if(isset($registro_ponto) ) $this->load->view("telas_ponto/".$registro_ponto);
                if(isset($inicio) ) $this->load->view("telas_login/".$inicio);               
            ?>
        </div>

<?php
$this->load->view("includes/footer.php");

