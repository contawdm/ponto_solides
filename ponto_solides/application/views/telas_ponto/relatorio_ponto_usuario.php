<script type="text/javascript">
	$(document).ready(function() {
        $('#enviar').click(function() {
            $.ajax({
	            url : "<?php echo site_url('index.php/crud/buscaPontoCompetencia')?>",
	            type: "POST",
	            data: $('#form').serialize(),
	            dataType: "JSON",
	            success: function(data)
	            {        
	              console.log(data);
	              	tb = "<button  class='btn btn-primary btn-small exportarExcel' ";
	              	tb += " title='Exportar para Excel' idTabela='table_ponto_mes'";
	              	tb += "  nomeArquivo='Relatório de ponto'> ";
	              	tb += " <i class='glyphicon glyphicon-download-alt' ></i> </button>";
	              	tb += " <br /><br />";             
    
	              	tb += "<table id='table_ponto_mes' class='table table-striped table-bordered' cellspacing='0' width='100%' >";
	              	tb+= "<tr>";
                	tb+= "	<th>DATA</th>";
                	tb+= "	<th>ENTRADA</th>";
                	tb+= "	<th>SAIDA ALMOÇO</th>";
                	tb+= "	<th>RETORNO ALMOÇO</th>";
                	tb+= "	<th>SAIDA</th>";
           			tb+= "</tr>";
        			tb+= "</thead>";

        			$.each(data, function (k, v) {
        				//console.log(k + " - "+ v+ " - "+ Object.keys(v) );   				
        				tb+= "<tr>";
                		tb+= "	<td>"+ k +"</td>";

        				count= 0;
        				hr= ["-", "-","-","-",];

        				$.each(v, function(kk, vv){
    						//console.log(k + " - "+ kk + " - "+ count);
    						hr[count] = kk;
    						count += 1;
        				});


        				tb+= "	<td>"+ hr[0] +"</td>";
        				tb+= "	<td>"+ hr[1] +"</td>";
        				tb+= "	<td>"+ hr[2] +"</td>";
        				tb+= "	<td>"+ hr[3] +"</td>";
        				tb+= "</tr>";
        			});

        			tb+= "</table>";
        			$('#retorno').append(tb);

	            },
	            error: function (jqXHR, textStatus, errorThrown)
	            {
	                alert('Erro ao  buscar os dados!');
	            }
	        });
        });
        
    });
</script>

<?php
	$opt = '';

	foreach($retorno as $key => $valor){ 
		$opt .= '<option  value="'.$valor->data.'">'.$valor->data.'</option>';
	}
?>


<div class="conteudo_principal" >
	<form action="#" id="form" >
		<div class="form-body lead">

			<div class="form-group" >
				<label class="control-label col-md-3">Selecione a competencia</label>
				<div class="col-md-3" >
					<select name="competencia" class="form-control" required>
						<?php echo $opt ;?>
					</select>
				</div>
			</div>

			<br /><br />
			<div class="form-group">
				<div class="col-md-3" ></div>
				<div class="col-md-3" >
					<input type="button" value="Enviar" id="enviar" class='btn btn-info' style="cursor:pointer"/>
					<input type="hidden" name="id_usuario" value="<?php echo $id_usuario ?>" />	
				</div>
			</div>
	</form>

	<br/>
	<br/>

	<div id="retorno"></div>
</div>