<script type="text/javascript">

    //para o datatable
    $(document).ready(function() {
        $('#table_usuarios').dataTable({
            "bPaginate": true,
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "todos"]],
            "language": {
                "url": "<?php echo base_url('assets/DataTableTradutorPortuguese-Brasil.json')?>"
            }
        });
    });
</script>

<div class="conteudo_principal" >
    <button  class="btn btn-primary btn-small exportarExcel" title="Exportar para Excel"
            idTabela="table_usuarios" nomeArquivo="Usuarios cadastrados">
        <i class="glyphicon glyphicon-download-alt" ></i>
    </button>
    <br /><br />

    <table id="table_usuarios" class="table table-striped table-bordered" cellspacing="0" width="100%" >
        <thead>
            <tr>
                <th>ID</th>
                <th>NOME</th>
                <th>LOGIN</th>
                <th>TIPO USUÁRIO</th>
                <th>STATUS</th>                
                <th>DATA CADASTRO</th>
                <th removeExcel>AÇÕES</th>
            </tr>
        </thead>
        <tbody>

    <?php
        foreach($retorno as $linha){ 
            $status = $linha->status_usuario == 1? '<font color="green">ATIVO</font>' : '<font color="red">DESATIVADO</font>';
            $data   = explode('-',$linha->dthr_cadastro_usuario);

            $upd_status = anchor("crud/troca_status_tipo_usuario/status/$linha->id_usuario/$linha->status_usuario","<button class='btn btn-primary' title='Alterar status'><i class='glyphicon glyphicon-pencil'  ></i></button>");

            $upd_tipo = anchor("crud/troca_status_tipo_usuario/tipo/$linha->id_usuario/$linha->tipo_usuario","<button class='btn btn-primary' title='Alterar tipo usuário'><i class='glyphicon glyphicon-user'  ></i></button>");

            $ponto = anchor("crud/relatorio_ponto/$linha->id_usuario","<button class='btn btn-primary' title='Relatório de Ponto do usuário {$linha->nome}'><i class='glyphicon glyphicon-calendar'  ></i></button>"); 

            echo "<tr class='even gradeC'>";
            echo "  <td>$linha->id_usuario</td>";
            echo "  <td>$linha->nome</td>";
            echo "  <td>$linha->login</td>";
            echo "  <td>$linha->tipo_usuario</td>";
            echo "  <td>$status</td>";
            echo "  <td>{$data[2]}/{$data[1]}/{$data[0]}</td>";
            echo "  <td nowrap >$upd_status $upd_tipo $ponto</td>";                
            echo "</tr>";
        }
    ?>

        </tbody>
        <tfoot></tfoot>
    </table> 
</div>