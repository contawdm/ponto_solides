<script type="text/javascript">
	//temporizador para retirar as mensagens de retorno
    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove(); 
        });
    }, 2000); 
</script>	

<div class="conteudo_principal" >
	
	<h3>Registrar Ponto</h3>

	<form method="post" action="<?php echo base_url('crud/registrarPonto'); ?>" class="navbar-form">
		<input type='hidden' value='<?php echo  $_SESSION['id_usuario'] ?>' name='id_usuario' />
		<input type="submit" class='btn btn-info ' value="Registrar Ponto" />
	</form>


	<?php
		if($this->session->flashdata("ponto_nao_registrado")){
	?>
			<div class="alert alert-danger" role="alert">
				<?php echo $this->session->flashdata("ponto_nao_registrado")?>
			</div>
	<?php
		}
		else if($this->session->flashdata("ponto_registrado")){
	?>
			<div class="alert alert-info" role="alert">
				<?php echo $this->session->flashdata("ponto_registrado")?>
			</div>
	<?php
		}
	?>


	<h3>Pontos registrados do Mês</h3>

	<table id="table_ponto_mes" class="table table-striped table-bordered" cellspacing="0" width="100%" >
        <thead>
            <tr>
                <th>DATA</th>
                <th>ENTRADA</th>
                <th>SAIDA ALMOÇO</th>
                <th>RETORNO ALMOÇO</th>
                <th>SAIDA</th>
            </tr>
        </thead>
        <tbody>
        	<?php 
        		//var_dump($retornoPontoMes);
        		$controle = $linha = null;


	        	foreach ($retornoPontoMes as $data =>$array ) {
	        		if(empty($controle)){
	        			$linha = "<tr><td>{$data}</td>";
	        			$controle = $data;
	        		} 
	        		else if($controle <> $data){
	        			$linha .= "</tr><tr><td>{$data}</td>";
	        			$controle = $data;
	        		}

	        		foreach ($array as $hora => $array2){
	        			$linha .= "	<td>{$hora}</td>";
	        			//echo "$data - $hora<br />";
	        		}	        						
	    		}   		

    		 	echo $linha;
        	?>
        </tbody>
   	</table>
	<br />
</div>