<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">

    <!-- para o layout responsivo-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title><?php echo $title ?></title>
    <?php  //echo link_tag('assets/teste_estilo.css') . "\n"; ?>
    
    <link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/datatables/css/dataTables.bootstrap.css')?>" rel="stylesheet">

    <script src="<?php echo base_url('assets/jquery/jquery-3.1.0.min.js')?>"></script>
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
    <script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js')?>"></script>
    <script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.js')?>"></script>

    <script src="<?php echo base_url('assets/exportadorExcel.js')?>"></script>
    
    
    <?php echo link_tag('assets/estilo_principal.css') . "\n"; ?>

<style>
.nav {
    display: block;
    float: left;
}

</style>
        
</head>
<body>


<nav class="navbar navbar-fixed-top navbar-inverse">
    <!--BARRA PARA LOGIN-->
    <div class="navbar-inner">
        <div class="container">
            <span class="lead" style="color: white;">Acesso Restrito 
                <?php 
                    if($_SESSION['nome']) echo " - ". $_SESSION['nome']." | ".
                    anchor("crud/deslogar","<button class='btn btn-danger' title='deslogar'><i class='glyphicon glyphicon-log-out'></i></button>"); 
                ?>
                <p class="lead" style="color: white;">SISTEMA DE PONTO <?php if($barraNome) echo " - ". $barraNome  ?></p>
            </span>


            <?php 
                    if($_SESSION['nome'] <> ''){
            ?>
            <div class="nav-collapse " >

                <ul class="nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="background-color: #000; color: #F00;">
                            cadastro
                            <b class="caret"></b>
                        </a>
                        <!-- lista dropdown-->
                        <ul class="dropdown-menu" style="background-color: #000; color: #F00;">
                            <li >
                                <a href="<?php echo base_url('crud/alterarDados/').$_SESSION['id_usuario']; ?>" target="_self">
                                    <font color="#F00">Alterar dados</font>
                                </a>
                            </li>
                            <li >
                                <a href="<?php echo base_url('crud/voltarRegistroPonto/').$_SESSION['id_usuario']; ?>" target="_self">
                                    <font color="#F00">Registro Ponto</font>
                                </a>
                            </li> 

                        <?php 
                            if($_SESSION['tipo_usuario'] == 'administrador'){
                        ?>

                            <li >
                                <a href="<?php echo base_url('crud/relatorioUsuarios') ?>" target="_self">
                                    <font color="#F00">Relatório Usuários/Ponto</font>
                                </a>
                            </li>                        

                        <?php 
                            }
                        ?>
                            <li >
                                <a href="<?php echo base_url('crud/deslogar') ?>" target="_self">
                                    <font color="#F00">Sair</font>
                                </a>
                            </li> 


                        </ul>                        

                    </li>
                </ul> 

                
                

            </div>

        <?php } ?>

        </div>
    </div>
    <!--fim da BARRA PARA LOGIN-->

    
</nav>
	
	
	<!-- CABEÇALHO -->
	<header class="jumbotron subhead"><!-- ver no css -->
		<div class="container">
			<p class="lead" style="color: white;">SISTEMA DE PONTO <?php if($barraNome) echo " - ". $barraNome  ?></p>
		</div>
	</header>
       
    <br /><br />