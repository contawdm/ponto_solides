
<div class="conteudo_principal" >
	<div class="alert alert-warning" role="alert">
		<h3>Faça o Login</h3>
	</div>
	
	<div >
		<form method="post" action="crud/verificaLogin" class="navbar-form">
			<div class="form-body lead">
				<div class="form-group " >
					<label class="control-label col-md-6" >Login</label>

					<div class="col-md-4" >	
						<input type="password" name="login" id="login" placeholder=" seu login de acesso" class="form-control" required />
					</div>
				</div>
				<br /><br />

				<div class="form-group " >
					<label class="control-label col-md-6" >Senha</label>

					<div class="col-md-4" >	
						<input type="password" name="password" id="password" placeholder="sua senha" class="form-control" required />
					</div>
				</div>	
				<br /><br />

				<div class="form-group">	
						
						<input type="submit" class='btn btn-info ' value="Enviar" />
						<?php echo anchor("crud/cadastrarLogin","<button class='btn btn-warning'>Fazer cadastro</button>"); ?>	
				</div>			
			</div>                          						
		</form>		
	<br />

	<?php
		if($this->session->flashdata("naoEncontrado")){
	?>
			<div class="alert alert-danger" role="alert">
				<?php echo $this->session->flashdata("naoEncontrado")?>
			</div>
	<?php
		}
	?>


</div>