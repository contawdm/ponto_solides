
<div class="conteudo_principal" >
	<h3>Preencha o cadastro</h3>
	<form action="cadastrarLogin2" method="POST">
		<div class="form-body lead">

			<div class="form-group" >
				<label class="control-label col-md-1">Nome</label>
				<div class="col-md-3" >
					<input type="text" name="nome" maxlength="50" size="30" class="form-control" required/>
				</div>
			</div>

			<br /><br />
			<div class="form-group " >
				<label class="control-label col-md-1" >Login</label>
				<div class="col-md-3" >
					<input type="text" name="login" maxlength="15" size="15" class="form-control" required />
				</div>
			</div>

			<br /><br />
			<div class="form-group " >
				<label class="control-label col-md-1" >Senha</label>
				<div class="col-md-3" >
					<input type="password" name="senha" maxlength="8" size="8" class="form-control" required />
				</div>
			</div>

			<br /><br />
			<div class="form-group " >
				<label class="control-label col-md-1" >tipo</label>
				<div class="col-md-3" >
					<select name="tipo" required >
						<option value="administrador">administrador</option>
						<option value="usuario">usuario</option>
					</select>
				</div>
			</div>

			<br /><br />
			<div class="form-group">
				<div class="col-md-1" ></div>
				<div class="col-md-3" >
					<input type="submit" value="Enviar" class='btn btn-info'/>		
					<?php 
						echo anchor("crud/deslogar","<button class='btn btn-success'>Voltar</button>"); 
					?>
				</div>
			</div>
		</div>
		<br /><br/>
		<?php
			if($this->session->flashdata("dadosEncontrados")){
		?>
				<div class="alert alert-danger" role="alert">
					<?php echo $this->session->flashdata("dadosEncontrados")?>
				</div>
		<?php
			}
		
			if($this->session->flashdata("cadastroOK")){
		?>
				<div class="alert alert-success" role="alert">
					<?php echo $this->session->flashdata("cadastroOK")?>
				</div>
		<?php
			}				
		?>		
	</form>
</div