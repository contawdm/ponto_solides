<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud_model extends CI_Model{
    function verificarLogin($array){
        $this->db->where("login",$array['login']);
        $this->db->where("password",$array['password']);
        $this->db->where("status_usuario = 1");       
		return $this->db->get("usuarios");
    }

    function inserirLogin($array){
        //echo "<pre>"; print_r($array);echo "</pre>"; die();
        $this->db->insert("usuarios",$array);
    }


    function buscarPontoMes($array){
        //echo "<pre>"; print_r($array);echo "</pre>"; die();
        $this->db->select("up.id_ponto, DATE_FORMAT(up.data_ponto , '%d/%m/%Y') AS data, DATE_FORMAT(up.hora_ponto , '%H:%i:%s') as hora");
        $this->db->from('usuarios_ponto up');        
        $this->db->where('up.id_usuario',$array['id_usuario']);
        $this->db->where('up.data_ponto >=',$array['competencia_inicio']);
        $this->db->where('up.data_ponto <=',$array['competencia_fim']);
        $this->db->order_by('up.id_ponto', 'ASC');
        return $this->db->get(); 
    }


    function verificaQuantidadeRegistrosDia($array){        
        $this->db->from('usuarios_ponto up');        
        $this->db->where('up.id_usuario',$array['id_usuario']);
        $this->db->where('up.data_ponto >=',$array['data']);
        return $this->db->count_all_results();
    }

    function registrarPonto($array){
        //echo "<pre>"; print_r($array);echo "</pre>"; die();
        $this->db->insert("usuarios_ponto",$array);
    }

    function buscarDadosUsuario($id_usuario){
        $this->db->where("id_usuario",$id_usuario);
        $this->db->where("status_usuario = 1");       
        return $this->db->get("usuarios");
    }


    function verificarAlterarLogin($array){
        $this->db->where("login",$array['login']);
        $this->db->where("password",$array['senha']);
        $this->db->where("status_usuario = 1"); 
        $this->db->where("id_usuario <>",$array['id_usuario']);     
        return $this->db->get("usuarios");
    }

    function atualizaLogin($array, $where){
        $this->db->update("usuarios", $array, $where);
        return $this->db->affected_rows();
    }


    function buscaUsuarios(){        
        return $this->db->get("usuarios");
    }


    function atualizarStatus($array, $where){
        $this->db->update("usuarios", $array, $where);
        return $this->db->affected_rows();
    }



    function buscaCompetenciasUsuario($id_usuario){     
        $this->db->select("DATE_FORMAT(up.data_ponto, '%m/%Y') AS data");
        $this->db->from('usuarios_ponto up');        
        $this->db->where('up.id_usuario',$id_usuario);
        $this->db->group_by('data');
        return $this->db->get();
    }


    

}