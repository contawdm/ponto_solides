<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ItensAvaria extends CI_Controller {

    public function __construct(){
		parent::__construct();//primeiro, chama o construtor da classe pai:
		//carregando os helpers no construtor
		$this->load->helper("url");
		$this->load->helper("form");
		$this->load->helper("html");
		$this->load->library("form_validation");
		$this->load->helper("array");
		$this->load->library("session");
		//carregar o model: alterado na aula 8 para ser usado por todas as funções
		$this->load->model("Avaria_model");
		//$this->load->library("table");
	}
	
	public function index()
	{
        
    }

    public function buscarItensAvariaId(){         
        $data = $this->Avaria_model->buscarItensAvariaId(array( "id"=>$this->uri->segment(3) ))->result();
        echo json_encode($data);
    }

    public function valoresGrafico(){        
        $dados = array(
            'title'=> 'Crud &raquo; selecionar avaria',
            'tela'=>'exibir_grafico',
            'barraNome'=>'Gráfico ItensAvarias cadastradas',
            'valoresItens'=>$this->Avaria_model->buscarValoresGrafico()->result()
        );  
        $this->load->view('crud',$dados);     
    }
    

    public function teste(){
        //para o teste via url passar http://localhost/CodeIgniter_3.1.7/index.php/crud/teste/fiat/uno
        $dados = array(
            'titulo'=>'teste de chamada',
            'texto'=> 'apresentação',
            'lista' => array(
                0 => 'abacaxi',
                1 => 'morango',
                2 => 'laranja'
            ),
            'url_marca' => $this->uri->segment(3),
            'url_modelo' => $this->uri->segment(4),
        );
        $this->load->view('teste',$dados);
    }
}