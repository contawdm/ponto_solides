<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud extends CI_Controller {

    public function __construct(){
		parent::__construct();
		//carregando os helpers no construtor
		$this->load->helper("url");
		$this->load->helper("form");
		$this->load->helper("html");
		$this->load->library("form_validation");
		$this->load->helper("array");
		$this->load->library("session");
		//carregar o model
		$this->load->model("Crud_model");
	}
	
	public function index()
	{
        $_SESSION['nome'] = '';
        $dados = array(
            'title'=> 'Ponto &raquo; Login',
            'inicio'=>'login',
            'barraNome'=>'login'
        );
		$this->load->view('crud',$dados);
    }

    public function cadastrarLogin(){
        $dados = array(
            'title'=> 'Ponto &raquo; Login  &raquo; Cadastro',
            'inicio'=>'login_cadastro',
            'barraNome'=>'cadastro de login'
        );
		$this->load->view('crud',$dados);

    }

    public function cadastrarLogin2(){
        //echo "<pre>"; print_r($_POST); echo "<pre>"; 

        //FORMATAÇÃO
        $_POST['nome'] = strtoupper($_POST['nome']);
                
        //VERIFICAÇÃO E INSERÇÃO
        $retorno= $this->Crud_model->verificarLogin($_POST)->row(); 

        if($retorno){
                $mens = "Login já cadastrado!";
                $this->session->set_flashdata("dadosEncontrados",$mens);
                redirect("crud/cadastrarLogin"); 
        }
        else{
            //insere
            $this->db->set('dthr_cadastro_usuario', 'NOW()', FALSE);
            $array = array("nome"=>$_POST['nome'], "tipo_usuario"=>$_POST['tipo'], "login"=>$_POST['login'], "password"=>md5($_POST['senha']), "status_usuario"=>'1');           
            $this->Crud_model->inserirLogin($array);

            
            $this->session->set_flashdata("cadastroOK","Cadastrado com sucesso!");
            redirect("crud/cadastrarLogin");
        }

    }

    public function deslogar(){
        unset($_SESSION['nome']);
        unset($_SESSION['tipo_usuario']);
        unset($_SESSION['id_usuario']);
        redirect(base_url());
    }

    public function verificaLogin(){ 
        $_POST['password'] = md5($_POST['password']);

        //VERIFICAÇÃO 
        $retorno= $this->Crud_model->verificarLogin($_POST)->row();
        //echo "<pre>"; print_r($retorno);echo "</pre>"; die();

        if($retorno) {
            $_SESSION['nome']           = $retorno->nome;
            $_SESSION['tipo_usuario']   = $retorno->tipo_usuario;
            $_SESSION['id_usuario']     = $retorno->id_usuario;

            $array = array('id_usuario'=>$retorno->id_usuario, 'data'=>date('Y-m-d'), 'competencia_inicio'=>date('Y-m-01'),  'competencia_fim'=> date('Y-m-31'));
            $this->registroPonto($array);
        }
        else{
            $this->session->set_flashdata("naoEncontrado","Dados não encontrados!");
            //redirect("crud/index"); //redireciona para o método index() do controller crud
            redirect(base_url());
        }
    }


    public function formatoExibirPontoMes($array){
        //var_dump($array);
        $formato = array();

        foreach ($array as $key => $value) {           
           $formato[$value->data][$value->hora]['hora']       = $value->hora; 
           $formato[$value->data][$value->hora]['id_ponto']   = $value->id_ponto; 
        }

        return $formato;
    }
      
    public function registroPonto($array){        
        $pontoMes = $this->formatoExibirPontoMes($this->Crud_model->buscarPontoMes($array)->result());

        $dados = array(
            'title'=> 'Ponto &raquo; Registro de ponto',
            'registro_ponto'=>'registro_ponto',
            'barraNome'=>'Ponto Registrados',
            'retornoPontoMes'=>$pontoMes
        );
        $this->load->view('crud',$dados);
    }


    public function registrarPonto(){ 
        //verifica qtd pontos dos dia               
        $array = array('id_usuario'=>$_POST['id_usuario'], 'data'=>date('Y-m-d'));

        if($this->Crud_model->verificaQuantidadeRegistrosDia($array) < 4){
           
            $this->db->set('data_ponto', 'NOW()', FALSE);   
            $this->db->set('hora_ponto', 'NOW()', FALSE);     
            $array = array("id_usuario"=>$_POST['id_usuario'] ); 

            $this->Crud_model->registrarPonto($array); 

            $this->session->set_flashdata("ponto_registrado","Registro inserido com sucesso!");           
        }
        else{
            $this->session->set_flashdata("ponto_nao_registrado","Já foram registrados as 4 macações de ponto diária!");
        }       

        //volta a pagina view/registro_ponto.php
        $array = array('id_usuario'=>$_POST['id_usuario'], 'data'=>date('Y-m-d'), 'competencia_inicio'=>date('Y-m-01'),  'competencia_fim'=> date('Y-m-31'));
        $this->registroPonto($array);
        
    }


    public function alterarDados($id_usuario = null){
        if($id_usuario == null) $id_usuario= $this->uri->segment(3);
        $dado=  $this->Crud_model->buscarDadosUsuario($id_usuario)->row();
       

        $dados = array(
            'title'=> 'Ponto &raquo; Atualiza cadastro',
            'inicio'=>'login_alterar',
            'barraNome'=>'Alterar Cadastro',
            'dadosusuario'=>$dado
        );
        $this->load->view('crud',$dados);
    }

     public function alterarLogin(){

        //FORMATAÇÃO
        $_POST['nome'] = strtoupper($_POST['nome']);
                
        //VERIFICAÇÃO E INSERÇÃO
        $retorno= $this->Crud_model->verificarAlterarLogin($_POST)->row(); 

        if($retorno){
                $mens = "Login já cadastrado para outro usuario!";

                $this->session->set_flashdata("dadosEncontrados",$mens);                
        }
        else{
            //altera            
            $array = array("nome"=>$_POST['nome'], "tipo_usuario"=>$_POST['tipo'], "login"=>$_POST['login'], "password"=>md5($_POST['senha']));  
            $where = array('id_usuario'=> $this->input->post('id_usuario') );         
            $this->Crud_model->atualizaLogin($array, $where);
            
            $this->session->set_flashdata("cadastroOK","Alterado com sucesso!");
            
        }

        $this->alterarDados($_POST['id_usuario']);      

     }


     function voltarRegistroPonto(){
        $array = array('id_usuario'=> $this->uri->segment(3), 'data'=>date('Y-m-d'), 'competencia_inicio'=>date('Y-m-01'),  'competencia_fim'=> date('Y-m-31'));
        $this->registroPonto($array);
     }


     function relatorioUsuarios(){
        $dados = array(
            'title'=> 'Ponto &raquo; Usuários Cadastrados',
            'registro_ponto'=>'relatorio_usuarios',
            'barraNome'=>'Relatório Usuários Cadastrados',
            'retorno'=>$this->Crud_model->buscaUsuarios()->result()
        );
        $this->load->view('crud',$dados);

     }


     function troca_status_tipo_usuario(){
        $grandeza_trocar    = $this->uri->segment(3);
        $id_usuario         = $this->uri->segment(4);
        $valor     = $this->uri->segment(5);
        
        if($grandeza_trocar == 'status'){
            if($valor == 1) $valor = 0;
            else $valor = 1;

            $this->Crud_model->atualizarStatus(array("status_usuario"=>$valor), array('id_usuario'=> $id_usuario) );
        }
        else  if($grandeza_trocar == 'tipo'){
            if($valor == 'administrador') $valor = 'usuario';
            else $valor = 'administrador';

            $this->Crud_model->atualizarStatus(array("tipo_usuario"=>$valor), array('id_usuario'=> $id_usuario) );

        }        

        $this->relatorioUsuarios();

     }
    
    
    function relatorio_ponto(){
        $id_usuario = $this->uri->segment(3);
        $competencias_usuario = $this->Crud_model->buscaCompetenciasUsuario($id_usuario)->result();
        
        $dados = array(
            'title'=> 'Ponto &raquo; Usuário Selecioando',
            'registro_ponto'=>'relatorio_ponto_usuario',
            'barraNome'=>'Relatório Usuário Selecionado',
            'id_usuario' => $id_usuario,
            'retorno'=>$competencias_usuario
        );
        $this->load->view('crud',$dados); 
    }    

    function buscaPontoCompetencia(){
        $data = explode('/',$this->input->post('competencia'));
        $competencia_inicio = "{$data[1]}-{$data[0]}-01";
        $competencia_fim = "{$data[1]}-{$data[0]}-31";        

        $array = array('id_usuario'=>$this->input->post('id_usuario'), 'competencia_inicio'=>$competencia_inicio,  'competencia_fim'=> $competencia_fim);

        //echo "<pre>"; print_r($array); echo "<pre>"; die();
        $retorno = $this->formatoExibirPontoMes($this->Crud_model->buscarPontoMes($array)->result());

        echo json_encode($retorno);
    }
}