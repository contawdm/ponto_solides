/*
Navicat MySQL Data Transfer

Source Server         : wel
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : ponto_solides

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2019-03-11 22:06:07
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `usuarios_ponto`
-- ----------------------------
DROP TABLE IF EXISTS `usuarios_ponto`;
CREATE TABLE `usuarios_ponto` (
  `id_ponto` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) unsigned NOT NULL,
  `data_ponto` date NOT NULL,
  `hora_ponto` time NOT NULL,
  PRIMARY KEY (`id_ponto`),
  KEY `fk_usuarios_ponto_id_usuarioXusuarios_id_usuario` (`id_usuario`),
  KEY `id_ponto` (`id_ponto`) USING BTREE,
  CONSTRAINT `fk_usuarios_ponto_id_usuarioXusuarios_id_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usuarios_ponto
-- ----------------------------
INSERT INTO `usuarios_ponto` VALUES ('1', '1', '2019-03-07', '09:08:39');
INSERT INTO `usuarios_ponto` VALUES ('2', '1', '2019-03-07', '11:08:56');
INSERT INTO `usuarios_ponto` VALUES ('3', '1', '2019-03-07', '12:09:09');
INSERT INTO `usuarios_ponto` VALUES ('4', '1', '2019-03-07', '17:14:34');
INSERT INTO `usuarios_ponto` VALUES ('5', '1', '2019-03-08', '09:15:17');
INSERT INTO `usuarios_ponto` VALUES ('6', '1', '2019-03-08', '11:25:39');
INSERT INTO `usuarios_ponto` VALUES ('7', '1', '2019-03-08', '12:15:23');
INSERT INTO `usuarios_ponto` VALUES ('8', '1', '2019-03-09', '08:16:20');
INSERT INTO `usuarios_ponto` VALUES ('9', '1', '2019-03-09', '13:15:12');
INSERT INTO `usuarios_ponto` VALUES ('10', '1', '2019-03-09', '22:56:38');
INSERT INTO `usuarios_ponto` VALUES ('11', '1', '2019-03-09', '23:04:28');
INSERT INTO `usuarios_ponto` VALUES ('12', '1', '2019-03-10', '09:20:11');
INSERT INTO `usuarios_ponto` VALUES ('13', '1', '2019-03-10', '09:39:36');
INSERT INTO `usuarios_ponto` VALUES ('14', '1', '2019-03-10', '09:42:15');
INSERT INTO `usuarios_ponto` VALUES ('15', '1', '2019-03-10', '09:44:50');
INSERT INTO `usuarios_ponto` VALUES ('16', '2', '2019-03-10', '19:22:25');
INSERT INTO `usuarios_ponto` VALUES ('17', '2', '2019-03-10', '19:24:13');
INSERT INTO `usuarios_ponto` VALUES ('18', '2', '2019-03-10', '19:25:37');
INSERT INTO `usuarios_ponto` VALUES ('19', '1', '2019-03-11', '22:03:56');
