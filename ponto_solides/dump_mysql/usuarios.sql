/*
Navicat MySQL Data Transfer

Source Server         : wel
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : ponto_solides

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2019-03-11 22:05:57
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `usuarios`
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id_usuario` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tipo_usuario` enum('usuario','administrador') NOT NULL DEFAULT 'usuario',
  `nome` varchar(50) NOT NULL,
  `login` varchar(15) NOT NULL,
  `password` char(32) NOT NULL,
  `status_usuario` char(1) NOT NULL,
  `dthr_cadastro_usuario` date NOT NULL,
  PRIMARY KEY (`id_usuario`),
  KEY `id_usuario` (`id_usuario`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usuarios
-- ----------------------------
INSERT INTO `usuarios` VALUES ('1', 'administrador', 'WELINGTON', 'wel', '202cb962ac59075b964b07152d234b70', '1', '2019-03-09');
INSERT INTO `usuarios` VALUES ('2', 'usuario', 'TESTE DA SILVA', 'teste', '698dc19d489c4e4db73e28a713eab07b', '1', '2019-03-09');
INSERT INTO `usuarios` VALUES ('3', 'administrador', 'JACK', 'jack', '4ff9fc6e4e5d5f590c4f2134a8cc96d1', '1', '2019-03-10');
